#include "LoadLibraryLinux.h"

#ifdef __linux__

#include <stdexcept>
#include <dlfcn.h>

namespace splugin
{
    void * SP_LoadLibrary(std::string &&filePath)
    {
        void * nh = dlopen(filePath.c_str(), RTLD_LAZY);
        if(nh == nullptr)
        {
            throw std::runtime_error("dlopen error " + std::string(dlerror()));
        }
        return nh;
    }
    
    void *SP_LoadFunction(void *nh, std::string &&funcName)
    {
        void * func = dlsym(nh, funcName.c_str());
        if(func == nullptr)
        {
            throw std::runtime_error("dlsym error " + std::string(dlerror()));
        }
        return func;
    }
    
    int SP_UnLoadLibrary(void *nh)
    {
        if(nh == nullptr) return -1;
        return dlclose(nh);
    }
}

#endif