#include "TypeA.h"

#include <iostream>


void TypeA::print()
{
    printf("print\n");
}

void TypeA::printStr(const std::string &str)
{
    std::cout << str << std::endl;
}


void TypeA::printInt(int i)
{
    printf("%d\n", i);
}

