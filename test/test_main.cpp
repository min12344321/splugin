#include <iostream>
#include <SPluginManager.h>

int main(){

    try
    {
        #ifdef __linux__
            auto p = splugin::SPluginManager::GetInstance().LoadPlugin("./libTypeA.so");
        #elif _WIN32
            auto p = splugin::SPluginManager::GetInstance().LoadPlugin("./TypeA.dll");
        #endif
        p->call("TypeA", "print");
        p->call("TypeA", "printInt", 1123);
        p->call("TypeA", "printStr", std::string("asd"));
        p->set("TypeA", "m_str", std::string("1234"));
        std::string str;
        p->get("TypeA", "m_str", str);
        std::cout << str << "\n";
        std::cout << "Hello, from SPlugin!\n";
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    


}



