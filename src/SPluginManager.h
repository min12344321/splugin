#ifndef _SPLUGIN_MANAGER_H_
#define _SPLUGIN_MANAGER_H_

#include <string>
#include <unordered_map>
#include <memory>

#include "SPluginBase.h"


namespace splugin
{

    class DXY_EXPORT_API SPluginManager 
    {
    public:
        static SPluginManager & GetInstance() { static SPluginManager instance; return instance; }

        SPluginBase * LoadPlugin(std::string && pluginPath);


    private:
        SPluginManager();
        virtual ~SPluginManager();
    
    private:
        std::unordered_map<std::string, std::pair<void *, SPluginBase *>>           m_Plugins;
    };

}


#endif