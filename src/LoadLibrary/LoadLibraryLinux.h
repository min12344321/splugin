#ifndef _LOADLIBRARY_LINUX_H_
#define _LOADLIBRARY_LINUX_H_


#ifdef __linux__

#include <string>

namespace splugin
{
    void * SP_LoadLibrary(std::string && filePath);

    void * SP_LoadFunction(void * nh, std::string && funcName);

    int SP_UnLoadLibrary(void * nh);
}

#endif


#endif