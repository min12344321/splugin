#ifndef _SPLUGIN_BASE_H_
#define _SPLUGIN_BASE_H_

#include <memory>
#include <dxyReflectClass.h>



namespace splugin
{
    #ifdef _WIN32
        #define DXY_EXPORT_API __declspec(dllexport)
        #define DXY_INPORT_API __declspec(dllinport)
    #elif __linux__
        #define DXY_EXPORT_API 
        #define DXY_INPORT_API
    #endif

    typedef void * (*CreateObject)();
    typedef void (*DeletObjct)(void *); 

    class DXY_EXPORT_API SPluginBase : public dxy::reflect::DxyRelfectObject 
    {
    public:
        virtual ~SPluginBase() {}

    };

    // #define CREATE_OBJ_REGISTER(className) extern "C" { void * CreateObject() { return dxy::reflect::DxyReflectClass::getInstance().getClassByName(#className).release(); } }
    // #define DELETE_OBJ_REGISTER(className) extern "C" { void DeleteObject(void * obj) { delete static_cast<className*>(obj); } } 

    #define OBJ_REGISTER(className) \
        extern "C" { DXY_EXPORT_API void * CreateObject() { return dxy::reflect::DxyReflectClass::getInstance().getClassByName(#className).release(); } } \
        extern "C" { DXY_EXPORT_API void DeleteObject(void * obj) { delete static_cast<className*>(obj); } } 
}

#endif