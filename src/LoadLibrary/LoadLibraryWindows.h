#ifndef _LOAD_LIBRARY_WINDOWS_H_
#define _LOAD_LIBRARY_WINDOWS_H_

#ifdef _WIN32

#include <string>

namespace splugin
{
    void * SP_LoadLibrary(std::string && filePath);

    void * SP_LoadFunction(void * nh, std::string && funcName);

    int SP_UnLoadLibrary(void * nh);
}


#endif


#endif