# SPlugin

#### 介绍

​	基于动态库显式调用和编译期反射实现的C++运行时反射。

#### 软件架构

* reflect 编译期反射库。
* Plugin 动态库显式调用。

#### 解决痛点

- 编译期反射无法支持类的运行时加载，每次扩展时要重新编译。
- 使用动态库显式调用对于动态库的编写必须要有一个接口类约束，不够方便灵活。

#### SPlugin的优势

- 结合编译期反射的接口灵活的优点和动态库显式调用的优势，完美解决上述痛点。

#### 安装教程

- 打开终端依次输入以下的命令

```bash
git clone https://gitee.com/min12344321/splugin.git
cd splugin
git submodule init && git submodule update
mkdir build && cd build
cmake ../
make
```

- 运行实例代码

#### 使用说明

- 参考示例代码
