#include "LoadLibraryWindows.h"

#ifdef _WIN32

#include <Windows.h>
#include <stdexcept>

namespace splugin
{
    void * SP_LoadLibrary(std::string &&filePath)
    {
        auto nh = LoadLibraryEx(filePath.c_str(), NULL, 0);
        if(nh == nullptr)
        {
            throw std::runtime_error("LoadLibrary error");
        }
        return nh;
    }
    
    void *SP_LoadFunction(void *nh, std::string &&funcName)
    {
        auto func = GetProcAddress((HMODULE)nh, funcName.c_str());
        if(func == nullptr)
        {
            throw std::runtime_error("GetProcAddress error");
        }
        return func;
    }
    
    int SP_UnLoadLibrary(void *nh)
    {
        if(nh == nullptr) return -1;
        return FreeLibrary((HMODULE)nh);
    }
}

#endif
