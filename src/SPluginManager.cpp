#include "SPluginManager.h"
#include "LoadLibraryLinux.h"
#include "LoadLibraryWindows.h"

namespace splugin
{


    SPluginBase * SPluginManager::LoadPlugin(std::string &&pluginPath)
    {
        if(m_Plugins.find(pluginPath) == m_Plugins.end())
        {
            m_Plugins[pluginPath].first = SP_LoadLibrary(std::move(pluginPath));
            auto func = (CreateObject)SP_LoadFunction(m_Plugins[pluginPath].first, "CreateObject");
            m_Plugins[pluginPath].second = static_cast<SPluginBase*>(func());
        }
        return m_Plugins[pluginPath].second;
    }

    SPluginManager::SPluginManager()
    {
    }

    SPluginManager::~SPluginManager()
    {
        for(auto & it : m_Plugins)
        {
            auto func = (DeletObjct)SP_LoadFunction(it.second.first, "DeleteObject");
            func(it.second.second);
            SP_UnLoadLibrary(it.second.first);
        }
    }




}