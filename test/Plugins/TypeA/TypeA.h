#ifndef _TYPE_A_H_
#define _TYPE_A_H_

#include <SPluginBase.h>
#include <dxyReflectClass.h>
#include <string>

class DXY_EXPORT_API TypeA : public splugin::SPluginBase
{
public:
    TypeA() { printf("TypeA\n"); }
    virtual ~TypeA() { printf("~TypeA\n"); }

    void print();

    void printStr(const std::string & str);
    
    void printInt(int i);
    
    std::string m_str;

};

OBJ_REGISTER(TypeA)
CLASS_REGISTER(TypeA)
CLASS_FIELD_REGISTER(TypeA, m_str)
CLASS_FUNC_REGISTER(TypeA, print)
CLASS_FUNC_REGISTER_ARGS(TypeA, printStr, const std::string)
CLASS_FUNC_REGISTER_ARGS(TypeA, printInt, int)




#endif